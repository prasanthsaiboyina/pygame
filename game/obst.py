class obstacles:
    def __init__(self, image, height, speed, distance):
        self.speed = speed
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = distance
        self.rect.y = height
    def move(self):
        self.rect.x = self.rect.x+self.speed
        if self.rect.x > 1000:
            self.rect.x = 0