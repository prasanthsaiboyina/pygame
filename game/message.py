import pygame

import time

screen = pygame.display.set_mode((1000, 998))

def message_display(text):
    Myfont = pygame.font.Font('freesansbold.ttf', 50)
    message = Myfont.render(text, False, (255, 255, 255))
    messagerect = message.get_rect()
    messagerect.center = (500,499)
    screen.fill((0, 0, 0))
    screen.blit(message, messagerect)
    pygame.display.update()
    time.sleep(2)

