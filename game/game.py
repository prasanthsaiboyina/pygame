import sys

import pygame

import time

import math

from config import *

from obst import *

from message import *

pygame.init()
pygame.font.init()


# ...................initialisation.................
myfont = pygame.font.Font('freesansbold.ttf', 30)
start = myfont.render('Start', False, (0, 0, 0))
end = myfont.render('End', False, (0, 0, 0))
score = myfont.render('SCORE', False, (0, 0, 0))

startrect = start.get_rect()
endrect = end.get_rect()

screen = pygame.display.set_mode(size)

herorect = hero.get_rect()

visited1 = [0]*10
visited2 = [0]*10
visit1 = 0
visit2 = 0


# ................obstacle_creation.............
obst = []
fixed = []


for x in range(5):
    o = obstacles(movobst, 83*(x+1)+15+100*x, var1, (x+1)*200)
    obst.append(o)


for x in range(1, 7):
    f = obstacles(fixobst, 100*x+83*x, var1, ((x+1)*256) % 1000)
    fixed.append(f)
    f = obstacles(fixobst, 100*x+83*x, var1, ((x+1)*150) % 1000)
    fixed.append(f)


# ............................positions_of_start_and_stop..........

herorect.x = player1_startx
herorect.y = player1_starty
startrect.x = player1_startx
startrect.y = player1_starty
endrect.x = player1_endx
endrect.y = player1_endy


# ...............scores............
score1 = 0
score2 = 0
flag1 = [5, 10, 5, 10, 5, 10, 5, 10, 5, 10]
flag2 = [10, 5, 10, 5, 10, 5, 10, 5, 10, 5]
score_1 = myfont.render(str(score1), False, (0, 0, 0))
int(score1)
score_2 = myfont.render(str(score2), False, (0, 0, 0))
int(score2)

ttemp1 = 0
ttemp2 = 0
time = pygame.time.get_ticks()
time_display = 0


# .................player_change.............
def player_change(score1, score2, temp1, temp2):
    if startrect.x == player1_startx:
        herorect.x = player2_startx
        herorect.y = player2_starty
        startrect.x = player2_startx
        startrect.y = player2_starty
        endrect.x = player2_endx
        endrect.y = player2_endy
        message_display(get_ready)
    else:
        for o in obst:
            o.speed = o.speed+1
        herorect.x = player1_startx
        herorect.y = player1_starty
        startrect.x = player1_startx
        startrect.y = player1_starty
        endrect.x = player1_endx
        endrect.y = player1_endy
        message_display(round_over)
        if score1/temp1 > score2/temp2:
            message_display(player1_wins)
        elif score1/temp1 < score2/temp2:
            message_display(player2_wins)
        else:
            message_display(draw)


# ................screen_refresh....................
def screen_remake(x1, y):
    for f in fixed:
        screen.blit(hero, (x1, y))
        for x in range(5):
            screen.blit(sea, (0, (x+1)*83+x*100))
        for x in range(6):
            screen.blit(land, (0, x*83+x*100))
        screen.blit(start, (startrect.x, startrect.y))
        screen.blit(end, (endrect.x, endrect.y))
        screen.blit(score, score_pos)
        screen.blit(time_displayed, time_position)
        if startrect.x == player1_startx:
            screen.blit(score_1, scoredis_pos)
        else:
            screen.blit(score_2, scoredis_pos)

    for o in obst:
        screen.blit(hero, (x1, y))
        for x in range(5):
            screen.blit(sea, (0, (x+1)*83+x*100))
        for x in range(6):
            screen.blit(land, (0, x*83+x*100))
        screen.blit(start, (startrect.x, startrect.y))
        screen.blit(end, (endrect.x, endrect.y))
        screen.blit(score, score_pos)
        screen.blit(time_displayed, time_position)

        if startrect.x == player1_startx:
            screen.blit(score_1, scoredis_pos)
        else:
            screen.blit(score_2, scoredis_pos)

    for o in obst:
        o.move()
        screen.blit(o.image, (o.rect.x, o.rect.y))
    for f in fixed:
        screen.blit(f.image, (f.rect.x, f.rect.y))


# ...................detection_of_collisions...............
def collision_detection(score1, score2, temp1, temp2):
    for f in fixed:
        if herorect.colliderect(f.rect):
            message_display(rock_col)
            player_change(score1, score2, temp1, temp2)

    for o in obst:
        if herorect.colliderect(o.rect):
            message_display(ship_col)
            player_change(score1, score2, temp1, temp2)
    if herorect.colliderect(endrect):
        message_display(success)
        player_change(score1, score2, temp1, temp2)


# .......................main_loop......................
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN and startrect.x == player2_startx:
            if event.key == pygame.K_a and herorect.x > 0:
                herorect.x = herorect.x-20
            if event.key == pygame.K_d and herorect.x < 945:
                herorect.x = herorect.x+20
            if event.key == pygame.K_w and herorect.y > 0:
                herorect.y = herorect.y-20
            if event.key == pygame.K_s and herorect.y < 943:
                herorect.y = herorect.y+20

        if event.type == pygame.KEYDOWN and startrect.x == player1_startx:
            if event.key == pygame.K_LEFT and herorect.x > 0:
                herorect.x = herorect.x-20
            if event.key == pygame.K_RIGHT and herorect.x < 945:
                herorect.x = herorect.x+20
            if event.key == pygame.K_UP and herorect.y > 0:
                herorect.y = herorect.y-20
            if event.key == pygame.K_DOWN and herorect.y < 943:
                herorect.y = herorect.y+20

    for o in obst:
        temp = o.speed
    if visit1 != temp:
        score1 = 0
        visit1 = temp
        score_1 = myfont.render(str(score1), False, (0, 0, 0))
        int(score1)

    if startrect.x == player1_startx:
        for i in range(1, 11):
            if herorect.y >= 83*math.ceil(i/2)+100*math.floor(i/2):
                if visited1[i-1] != o.speed:
                    visited1[i-1] = o.speed
                    score1 = score1+flag1[i-1]
                    score_1 = myfont.render(str(score1), False, (0, 0, 0))
                    int(score1)

    if visit2 != temp:
        score2 = 0
        visit2 = temp
        score_2 = myfont.render(str(score2), False, (0, 0, 0))
        int(score2)

    if startrect.x == player2_startx:
        for i in range(1, 11):
            if herorect.y <= 83*math.ceil(i/2)+100*math.floor(i/2):
                if visited2[i-1] != o.speed:
                    visited2[i-1] = o.speed
                    score2 = score2+flag2[i-1]
                    score_2 = myfont.render(str(score2), False, (0, 0, 0))
                    int(score2)

    if startrect.x == player1_startx:
        current_time1 = pygame.time.get_ticks()
        if temp == 1:
            time_display = math.floor((current_time1-time)/1000)
            ttemp1 = (current_time1-time)/1000
        else:
            time_display = math.floor((current_time1-current_time2)/1000)-6
            ttemp1 = ((current_time1-current_time2)/1000)-6
    if startrect.x == player2_startx:
        current_time2 = pygame.time.get_ticks()
        time_display = math.floor((current_time2-current_time1)/1000)-4
        ttemp2 = ((current_time2-current_time1)/1000)-4

    time_displayed = myfont.render(str(time_display), False, (0, 0, 0))
    screen.blit(time_displayed, time_position)
    pygame.display.update()
    collision_detection(score1, score2, ttemp1, ttemp2)
    screen_remake(herorect.x, herorect.y)
    screen.blit(hero, (herorect.x, herorect.y))
    pygame.display.update()
