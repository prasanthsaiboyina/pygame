import pygame

size = width, height = 1000, 998
var1 = 1
blue = 0, 105, 148
sand = 237, 201, 175

sea = pygame.Surface((1000, 100))
sea.fill(blue)
land = pygame.Surface((1000, 83))
land.fill(sand)

player1_startx = 700
player1_starty = 0
player1_endx = 200
player1_endy = 943
player2_startx = 200
player2_starty = 943
player2_endx = 700
player2_endy = 0

time_position = (900, 0)

movobst = pygame.image.load("images/ship.png")
fixobst = pygame.image.load("images/rock.png")

hero = pygame.image.load("images/hero.png")

score_pos = 0, 0
scoredis_pos = 120, 0

player1_wins = 'PLAYER1 WINS THIS ROUND'
player2_wins = 'PLAYER2 WINS THIS ROUND'

get_ready = 'Player 2 get Ready'
round_over = 'Round is over'
draw = 'TIE'

rock_col = 'Collision With rock'
ship_col = 'Collision With ship'
success = 'You completed this round'